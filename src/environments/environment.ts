// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
    firebaseConfig : {
    apiKey: "AIzaSyA911_FejdY2s1OYNn5ggMp-aKPafif8Wc",
    authDomain: "clima-61346.firebaseapp.com",
    databaseURL: "https://clima-61346.firebaseio.com",
    projectId: "clima-61346",
    storageBucket: "clima-61346.appspot.com",
    messagingSenderId: "122713752939",
    appId: "1:122713752939:web:f4d380ef099c4691bc782e",
    measurementId: "G-YFWV1Y8FD8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
